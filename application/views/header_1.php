<!DOCTYPE html>
<html lang="en">
<head>
  <title>Devsteam Ltd</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="<?php echo base_url('css/bootstrap.min.css')?>">
  
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
  <div class="container-fluid">
      <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand" href="#">DevesTeam</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="<?php echo site_url('user_controller/user_login'); ?>">Home</a></li>
                <li class=""><a href="#">About Us</a></li>
                <li class=""><a href="#">Contact Us</a></li>
            </ul>
           
            <ul class="nav navbar-nav navbar-right">
                <?php if(isset($_SESSION)){ ?>
                
                <?php if($this->uri->segment(1)==="admin_controller"){?>
                <li><a href="<?php echo site_url('admin_controller/logout');?>"><span class="glyphicon glyphicon-user"></span> <?php echo ucfirst($_SESSION['username']);?> | Logout</a></li>
                <?php }else if($this->uri->segment(1)==="user_controller"){?>
                <li><a href="<?php echo site_url('user_controller/logout');?>"><span class="glyphicon glyphicon-user"></span> <?php echo ucfirst($_SESSION['username']);?> | Logout</a></li>                

                <?php }}else{?>
                <li><a href="<?php echo site_url('user_controller/load_register');?>"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                <li><a href="<?php echo site_url('user_controller/');?>"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>

                <?php }?>
            </ul>

        </div>
    </div>
  </div>
</nav>
    <br><br><br>
 