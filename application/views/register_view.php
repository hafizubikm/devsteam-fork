<?php include_once('header_1.php');?>
<div class="container">
  <div class="col-md-3">
      
  </div>
    <div class="col-md-6 col-xs-12">
        <div class="panel-group">
    <div class="panel panel-info">
      <div class="panel-heading"><h4>Login Here</h4></div>
      <div class="panel-body">
          <?php echo form_open("user_controller/register") ;?>
              <div class="row">
                  <div class="col-md-12">
              <label>Name :</label><br>
              <input type="text" class="form-control" required="true" name="name" placeholder="Your Name"><br>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6">
                    <label>Username :</label><br>
                    <input type="text" class="form-control" required="true" name="username" placeholder="Your Username">
                  </div>
                  <div class="col-md-6">
                    <label>Password :</label><br>
                    <input type="password" class="form-control" required="true" name="password" placeholder="Your Password"><br>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6">
                    <label>Email :</label><br>
                    <input type="text" class="form-control" required="true" name="email" placeholder="Your Email">
                  </div>
                  <div class="col-md-6">
                      <label>Phone :</label><br>
                      <input type="text" class="form-control" required="true" name="phone" placeholder="Your Phone"><br>
                  </div>
              </div>
              <div class="row">
                  <div class="col-md-6">
                    <label>District :</label><br>
                    <select class="form-control" name="district">
                        <option value="">Select Your District</option>
                        <option value=1>Dhaka</option>
                        <option value=2>Brahmanbaria</option>
                        <option value=3>Gazipur</option>
                    </select>
                  </div>
                  <div class="col-md-6">
                      <label>Thana :</label><br>
                      <select class="form-control" name="thana">
                          <option value="">Select Your Thana</option>
                          <option value=1>Sarail</option>
                          <option value=2>Nasir Nagar</option>
                          <option value=3>Ashugang</option>
                          <option value=4>Kasba</option>
                      </select>
                  </div>
              </div><br>
              <div class="row">
                  <div class="col-md-12">
                      <label>Enter Your address</label><br>
                      <textarea class="form-control" name="address"></textarea>
                  </div>                  
              </div><br>
              <div class="row">
                  <div class="col-md-6">
                      
                  </div>
                  <div class="col-md-6">
                      <label>Submit Your Registration</label><br>
                      <input type="submit" class="btn btn-info form-control" value="Submit">
                  </div>
              </div>  <br>
              <div class="row">
                  <div class="col-md-12">
                      <a href="<?php echo site_url('user_controller/user_login') ?>">Have You Alerady An Account?</a>
                  </div>                  
              </div>
          </forn>
      </div>
    </div>
   </div>
    </div>
    <div class="col-md-3">
      
    </div>
</div>

</body>
</html>
