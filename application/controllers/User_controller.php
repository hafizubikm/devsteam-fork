<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of User
 *
 * @author User
 */

class User_controller extends CI_Controller {
    //put your code here
    
    
    public function index(){
         $this->load->view('user_login');
    }
    
    
    public function load_register(){
         $this->load->view('register_view');
    }
    public function wellcome_user(){
        $this->load->view('welcome_user_view');
    }


    public function register(){
        $data['name']=$this->input->post('name'); // here $data['name'] name is database tables name and post('name') is form input name
        $data['username']=$this->input->post('username');
        $data['password']=$this->input->post('password');
        $data['email']=$this->input->post('email');
        $data['phone']=$this->input->post('phone');
        $data['dist_id']=$this->input->post('district');
        $data['thana_id']=$this->input->post('thana');
        $data['address']=$this->input->post('address');
        $this->load->model('user_model'); //user_model.php is loaded from model
        $this->user_model->register($data); // call register method from user_model 
        redirect('user_controller/wellcome_user');
    }
    public function display_user(){
        $this->load->model("user_model");
        $data['users']=$this->user_model->get_user();
        $this->load->view('display_user',$data);
    }
    
    public function user_login(){
        $this->load->model('user_model');
        $check=$this->user_model->get_login_check($this->input->post('username')); // here 'username' is html name="username"
       if($check>0){  
           session_start();
           $_SESSION['username']=$this->input->post('username');
           $this->load->view('user_projects');
       }else{
           $this->load->view('user_login');
       }        
    }
    
    public function logout(){
        session_destroy();
        redirect('user_controller');
    }
}?>