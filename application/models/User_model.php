<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class User_model extends CI_Model {

  function __construct()
    {
        parent::__construct();
    }
    
    public function register($data){
        $this->db->insert("user",$data); // here 'user' is a table of database & insert() method return a individual column based query
    }
    
    public function get_user(){
       $query= $this->db->get("user"); // here 'user' is a table of database
       return $query->result();        
    }
    public function get_login_check($username){
       $query=$this->db->get_where('user',array('username'=>$username));
        return $query->num_rows();
    }    
}
