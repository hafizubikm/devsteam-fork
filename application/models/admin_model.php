<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_model extends CI_Model {

  function __construct()
    {
        parent::__construct();
    }
    
       
    public function get_admin(){
       $query= $this->db->get("admin"); // here 'admin' is a table of database
       return $query->result();
    }
    public function get_admin_check($adminname){
       $query=$this->db->get_where('admin',array('admin_name'=>$adminname));
        return $query->num_rows();
    }
    public function get_user(){
       $query= $this->db->get("user"); // here 'user' is a table of database
       return $query->result();        
    }
}
